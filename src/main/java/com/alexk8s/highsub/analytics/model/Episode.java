package com.alexk8s.highsub.analytics.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Column;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Table(name = "episode")
@Data
public class Episode {

    @Id
    @GeneratedValue
    private UUID id;
    private String show;
    private String season;
    private String episode;

    @Column("english_subtitles")
    private String englishSubtitles;
    @Column("japanese_subtitles")
    private String japaneseSubtitles;
    @Column("subtitle_count")
    private Long subtitleCount;
    private String summary;
    private String studio;
    @Column("opening_start")
    private Long openingStart;
    @Column("opening_end")
    private Long openingEnd;
    @Column("ending_start")
    private Long endingStart;
    @Column("ending_end")
    private Long endingEnd;

    @Column("prologue_start")
    private Long prologueStart;
    @Column("prologue_end")
    private Long prologueEnd;
    @Column("preview_start")
    private Long previewStart;
    @Column("preview_end")
    private Long previewEnd;
    @Column("originally_available_at")
    private String originallyAvailableAt;
    private String title;
    @Column("source_video")
    private String sourceVideo;
    @Column("source_video_bytes")
    private Long sourceVideoBytes;


    @Column("scaled_480p_video")
    private String scaled480pVideo;
    @Column("scaled_150p_video")
    private String scaled150pVideo;
    @Column("scaled_480p_video_bytes")
    private Long scaled480pVideoBytes;
    @Column("scaled_150p_video_bytes")
    private Long scaled150pVideoBytes;

    private List<String> tags;
    @Column("parent_tags")
    private List<String> parentTags;
    private Integer width;
    private Integer height;
    private Long duration;
    @Version
    private Long version;




}
