package com.alexk8s.highsub.analytics.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Column;

import java.util.List;

@Table(name = "SHOW")
@Data
@Entity
public class Show {
    @Column( "show")
    @Id
    private String show;
    @Column( "is_anime")
    private Boolean anime;
    @Column( "kitsu_anime_id")
    private String kitsuAnimeId;
    @Column( "plex_key")
    private String plexKey;
    @Column( "show_year")
    private Integer showYear;
    @Column( "summary")
    private String summary;
    @Column( "tags")
    private List<String> tags;
    @Column( "tmdb_episode_group")
    private String tmdbEpisodeGroup;
    @Column( "tmdb_show_id")
    private Integer tmdbShowId;
    @Version
    private Long version;
}
