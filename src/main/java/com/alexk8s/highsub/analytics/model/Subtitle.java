package com.alexk8s.highsub.analytics.model;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Column;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name="subtitle")
@Data
@With
@AllArgsConstructor
@NoArgsConstructor
public class Subtitle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String show;
    private String season;
    private String episode;
    private Integer subtitleNumber;

    @Column("start_time")
    private Long startTime;
    @Column("end_time")
    private Long endTime;
    private String style;
    private String name;
    private String text;
    @Column("created_utc")
    private Long createdUtc;
    private String kanji;
    private String romaji;
    @Column("japanese_start_time")
    private Long japaneseStartTime;
    @Column("japanese_end_time")
    private Long japaneseEndTime;
    @Column("japanese_subtitle_number")
    private Integer japaneseSubtitleNumber;
    @Column("match_rate")
    private String matchRate;
    private List<String> tags;
    @Column("parent_tags")
    private List<String> parentTags;
    private List<String> context;
    @Column("raw_subtitle")
    private List<String> rawSubtitle;
    @Version
    private Long version;
    private String hash;
    @Column("indexed_id")
    private String indexedId;
    private String chapter;
    @Column("should_index")
    private Boolean shouldIndex;
    @Enumerated(EnumType.STRING)
    private SubtitleType type;
}
