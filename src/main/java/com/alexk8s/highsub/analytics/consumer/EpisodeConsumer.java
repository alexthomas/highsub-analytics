package com.alexk8s.highsub.analytics.consumer;

import com.alexk8s.highsub.analytics.model.Episode;
import com.alexk8s.highsub.analytics.repository.EpisodeRepository;
import com.alexk8s.highsub.kafka.model.EpisodeV1;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

import static com.alexk8s.highsub.analytics.util.KafkaUtils.ackMessage;

@Component
@AllArgsConstructor
@Slf4j
public class EpisodeConsumer implements Consumer<Flux<Message<EpisodeV1>>> {
    private final EpisodeRepository episodeRepository;
    @Override
    public void accept(Flux<Message<EpisodeV1>> messageFlux) {
        messageFlux.flatMap(this::handleMessage,1).onErrorContinue((t,o)->{
            log.error("Error processing message",t);
        }).subscribe();
    }

    private Mono<Void> handleMessage(Message<EpisodeV1> message){
        EpisodeV1 episodeV1 = message.getPayload();
        Mono<?> publisher = switch (episodeV1.getType$1()){
            case CREATE -> episodeRepository.upsert(convertEpisode(episodeV1));
            case SCALE -> episodeRepository.findByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode())
                    .flatMap(episode -> {
                        episode.setScaled150pVideo(episodeV1.getScaled150pVideo());
                        episode.setScaled150pVideoBytes(episodeV1.getScaled150pVideoBytes());
                        episode.setScaled480pVideo(episodeV1.getScaled480pVideo());
                        episode.setScaled480pVideoBytes(episodeV1.getScaled480pVideoBytes());
                        return episodeRepository.save(episode);
                    });
            case UPDATE_JAPANESE_SUBTITLES -> episodeRepository.findByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode())
                    .flatMap(episode -> {
                        episode.setJapaneseSubtitles(episodeV1.getJapaneseSubtitles());
                        return episodeRepository.save(episode);
                    });
            case UPDATE_SUBTITLE_COUNT -> episodeRepository.findByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode())
                    .flatMap(episode -> {
                        episode.setSubtitleCount(episodeV1.getSubtitleCount());
                        return episodeRepository.save(episode);
                    });
            case UPDATE_TAGS -> episodeRepository.findByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode())
                    .flatMap(episode -> {
                        episode.setTags(episodeV1.getTags());
                        episode.setParentTags(episodeV1.getParentTags());
                        return episodeRepository.save(episode);
                    });
            case UPDATE_CHAPTERS -> episodeRepository.findByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode())
                    .flatMap(episode -> {
                        episode.setOpeningStart(episodeV1.getOpeningStart());
                        episode.setOpeningEnd(episodeV1.getOpeningEnd());
                        episode.setEndingStart(episodeV1.getEndingStart());
                        episode.setEndingEnd(episodeV1.getEndingEnd());
                        episode.setPrologueStart(episodeV1.getPrologueStart());
                        episode.setPrologueEnd(episodeV1.getPrologueEnd());
                        episode.setPreviewStart(episodeV1.getPreviewStart());
                        episode.setPreviewEnd(episodeV1.getPreviewEnd());
                        return episodeRepository.save(episode);
                    });
            case DELETE -> episodeRepository.deleteByShowAndSeasonAndEpisode(episodeV1.getShow(), episodeV1.getSeason(), episodeV1.getEpisode());
            case REINDEX, RESCALE -> Mono.empty();
        };
        return publisher.doOnSuccess((x)->ackMessage(message)).then();
    }

    private Episode convertEpisode(EpisodeV1 episodeV1){
        Episode episode = new Episode();
        BeanUtils.copyProperties(episodeV1, episode);
        return episode;
    }
}
