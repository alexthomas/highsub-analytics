package com.alexk8s.highsub.analytics.consumer;

import com.alexk8s.highsub.analytics.model.Show;
import com.alexk8s.highsub.analytics.repository.ShowRepository;
import com.alexk8s.highsub.kafka.model.ShowV1;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.alexk8s.highsub.analytics.util.KafkaUtils.ackMessage;

@Component
@Slf4j
@AllArgsConstructor
public class ShowConsumer implements Consumer<Flux<Message<ShowV1>>> {
    private final ShowRepository showRepository;

    @Override
    public void accept(Flux<Message<ShowV1>> messageFlux) {
        messageFlux.flatMap(this::processMessage,1).subscribe();
    }

    private Mono<Void> processMessage(Message<ShowV1> message){
        ShowV1 showV1 = message.getPayload();
        Mono<?> publisher = switch (showV1.getType$1()){
            case CREATE -> showRepository.upsert(convertShow(showV1));
            case UPDATE_ANIME -> showRepository.findById(showV1.getShow())
                    .flatMap(show -> {
                        show.setAnime(showV1.getAnime());
                        return showRepository.save(show);
                    });
            case UPDATE_TAGS -> showRepository.findById(showV1.getShow())
                    .flatMap(show -> {
                        show.setTags(showV1.getTags());
                        return showRepository.save(show);
                    });
            case UPDATE_KITSU_ID -> showRepository.findById(showV1.getShow())
                    .flatMap(show -> {
                        show.setKitsuAnimeId(showV1.getKitsuAnimeId());
                        return showRepository.save(show);
                    });
            case UPDATE_SUMMARY -> showRepository.findById(showV1.getShow())
                    .flatMap(show -> {
                        show.setSummary(showV1.getSummary());
                        return showRepository.save(show);
                    });
            case DELETE -> showRepository.deleteById(showV1.getShow());
        };
        return publisher.doOnSuccess((x)->ackMessage(message)).then();
    }

    private Show convertShow(ShowV1 showV1){
        Show show = new Show();
        BeanUtils.copyProperties(showV1,show);
        return show;
    }


}
