package com.alexk8s.highsub.analytics.consumer;

import com.alexk8s.highsub.analytics.model.Subtitle;
import com.alexk8s.highsub.analytics.repository.SubtitleRepository;
import com.alexk8s.highsub.analytics.util.HashUtil;
import com.alexk8s.highsub.kafka.model.SubtitleV3;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

import static com.alexk8s.highsub.analytics.util.KafkaUtils.ackMessage;

@Component
@Slf4j
@AllArgsConstructor
public class SubtitleConsumer implements Consumer<Flux<Message<SubtitleV3>>> {
    private final SubtitleRepository subtitleRepository;
    @Override
    public void accept(Flux<Message<SubtitleV3>> messageFlux) {
        messageFlux.flatMapSequential(this::handleMessage,1).onErrorContinue((t,o)->{
            log.error("Error processing message {}",o,t);
        }).subscribe();
    }

    private Mono<Void> handleMessage(Message<SubtitleV3> message){
      SubtitleV3 subtitleV3 = message.getPayload();
            Mono<?> publisher = switch (subtitleV3.getType$1()){
                case CREATE -> subtitleRepository.upsert(convertSubtitle(subtitleV3));
                case DELETE -> subtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(subtitleV3.getShow(), subtitleV3.getSeason(), subtitleV3.getEpisode(), subtitleV3.getSubtitleNumber())
                        .flatMap(subtitleRepository::delete);
                case UPDATE_TAGS -> subtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(subtitleV3.getShow(), subtitleV3.getSeason(), subtitleV3.getEpisode(), subtitleV3.getSubtitleNumber())
                        .flatMap(subtitle -> {
                            subtitle.setTags(subtitleV3.getTags());
                            subtitle.setParentTags(subtitleV3.getParentTags());
                            return subtitleRepository.save(subtitle);
                        });
            };
            return publisher.doOnSuccess((x)->ackMessage(message)).then();
    }

    private Subtitle convertSubtitle(SubtitleV3 subtitleV3){
        Subtitle subtitle = new Subtitle();
        BeanUtils.copyProperties(subtitleV3, subtitle);
        subtitle.setHash(HashUtil.hashStrings(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(),subtitle.getSubtitleNumber()+"", subtitle.getText()));
        return subtitle;
    }
}
