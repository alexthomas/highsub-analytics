package com.alexk8s.highsub.analytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighsubAnalyticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HighsubAnalyticsApplication.class, args);
    }

}
