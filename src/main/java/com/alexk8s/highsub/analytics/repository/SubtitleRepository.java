package com.alexk8s.highsub.analytics.repository;

import com.alexk8s.highsub.analytics.model.Subtitle;
import org.springframework.beans.BeanUtils;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface SubtitleRepository extends R2dbcRepository<Subtitle, UUID> {

    Mono<Subtitle> findByShowAndSeasonAndEpisodeAndSubtitleNumber(String show, String season, String episode, Integer subtitleNumber);

    default Mono<Subtitle> upsert(Subtitle subtitle) {
        return findByShowAndSeasonAndEpisodeAndSubtitleNumber(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(), subtitle.getSubtitleNumber())
                .flatMap(existingSubtitle -> {
                    BeanUtils.copyProperties(subtitle, existingSubtitle, "version", "id");
                    return save(existingSubtitle);
                })
                .switchIfEmpty(save(subtitle)).retry(2);
    }

}
