package com.alexk8s.highsub.analytics.repository;


import com.alexk8s.highsub.analytics.model.Show;
import org.springframework.beans.BeanUtils;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface ShowRepository extends R2dbcRepository<Show, String> {

    default Mono<Show> upsert(Show show){
        return findById(show.getShow())
                .flatMap(existingShow -> {
                    BeanUtils.copyProperties(show,existingShow,"version");
                    return save(existingShow);
                })
                .switchIfEmpty(save(show));
    }

}
