package com.alexk8s.highsub.analytics.repository;


import com.alexk8s.highsub.analytics.model.Episode;
import com.alexk8s.highsub.analytics.model.Show;
import org.springframework.beans.BeanUtils;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface EpisodeRepository extends R2dbcRepository<Episode, UUID> {

    Mono<Episode> findByShowAndSeasonAndEpisode(String show, String season, String episode);
    Mono<Void> deleteByShowAndSeasonAndEpisode(String show, String season, String episode);
    default Mono<Episode> upsert(Episode episode){
        return findByShowAndSeasonAndEpisode(episode.getShow(),episode.getSeason(),episode.getEpisode())
                .flatMap(existingEpisode -> {
                    BeanUtils.copyProperties(episode,existingEpisode,"version","id");
                    return save(existingEpisode);
                })
                .switchIfEmpty(save(episode));
    }

}
