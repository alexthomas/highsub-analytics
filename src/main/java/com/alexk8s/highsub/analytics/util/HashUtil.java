package com.alexk8s.highsub.analytics.util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class HashUtil {

    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String hashStrings(String... strings) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            for (String string : strings) {
                digest.update(string.getBytes(StandardCharsets.UTF_8));
            }
            return digest(digest);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String digest(MessageDigest digest) {
        byte[] bytes = digest.digest();
        int byteCount = Math.min(bytes.length, 32);
        char[] hexChars = new char[byteCount * 2];
        for (int j = 0; j < byteCount; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
