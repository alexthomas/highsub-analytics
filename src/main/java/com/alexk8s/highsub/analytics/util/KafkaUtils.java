package com.alexk8s.highsub.analytics.util;

import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import reactor.kafka.receiver.ReceiverOffset;



public class KafkaUtils {
    public static void ackMessage(Message<?> message){
        ReceiverOffset kafkaAcknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, ReceiverOffset.class);
        if (kafkaAcknowledgment != null)
            kafkaAcknowledgment.acknowledge();
    }

}
