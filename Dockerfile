FROM openjdk:17-slim-buster
EXPOSE 8080
COPY build/libs/analytics.jar analytics.jar
ENTRYPOINT java -jar analytics.jar
